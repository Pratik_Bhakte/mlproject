from setuptools import find_packages, setup
from typing import List

HYFEN_E_DOT = "-e ."
def get_requirements(file_path:str)->List[str]:
    requirements=[]
    with open(file_path) as file_obj:
        requirements = file_obj.readlines()
        requirements = [req.replace("\n", "") for req in file_obj]
        if HYFEN_E_DOT in requirements:
            requirements.remove(HYFEN_E_DOT)
    return requirements
setup(
    name='mlproject',
    version='0.0.10',
    author='Pratik',
    author_email='pratikbhakte9@gmail.com',
    packages=find_packages(),
    install_requires=get_requirements('requirements.txt')
)
